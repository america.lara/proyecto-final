import React, { useMemo } from "react";
import { Image } from "expo-image";

import { StyleSheet, Text, View, ImageSourcePropType } from "react-native";
import { Border, FontSize, FontFamily, Color, Padding } from "../GlobalStyles";
import { TouchableOpacity } from "react-native-gesture-handler";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const UserAvatarContainer = ({ itemCode, propMarginTop }) => {
  const avatarStyle = useMemo(() => {
    
    return {
      
      ...getStyleValue("marginTop", propMarginTop),
    };
  }, [propMarginTop]);

  return (
    <View style={[styles.avatar, avatarStyle]}>
      <Image style={styles.avatarIcon} contentFit="cover" source={itemCode} />
      <View style={styles.titleWrapper}>
        <Text style={styles.title} numberOfLines={1}>
          Jordan Vidaña
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  avatarIcon: {
    borderRadius: Border.br_21xl,
    width: 40,
    height: 40,
    overflow: "hidden",
  },
  title: {
    fontSize: FontSize.size_base,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
  },
  titleWrapper: {
    marginTop: 6,
    marginLeft: 12,
  },
  avatar: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingHorizontal: Padding.p_xs,
    marginTop: 12,
    alignSelf: "auto",
  },
});

export default UserAvatarContainer;
