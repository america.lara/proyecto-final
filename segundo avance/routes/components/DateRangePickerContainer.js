import * as React from "react";
import { Text, StyleSheet, View, ImageSourcePropType } from "react-native";
import { Image } from "expo-image";
import { FontFamily, Color, FontSize, Padding } from "../GlobalStyles";

const DateRangePickerContainer = ({ dateText, eventDate, eventCode }) => {
  return (
    <View style={[styles.list, styles.listFlexBox]}>
      <View style={[styles.item, styles.listFlexBox]}>
        <View style={styles.titleParent}>
          <Text style={[styles.title, styles.subtitleTypo]}>Zona Centro</Text>
          <Text style={[styles.subtitle, styles.subtitleTypo]}>Matutino</Text>
        </View>
        <View style={[styles.titleGroup, styles.titleSpaceBlock]}>
          <Text style={[styles.title, styles.subtitleTypo]}>Conductor</Text>
          <Text style={[styles.subtitle, styles.subtitleTypo]}>
            Kevin Mendoza
          </Text>
          <Text style={[styles.subtitle2, styles.subtitleTypo]}>
            Transporte
          </Text>
          <Text style={[styles.subtitle, styles.subtitleTypo]}>101</Text>
        </View>
        <View style={[styles.titleContainer, styles.titleSpaceBlock]}>
          <Text style={[styles.title, styles.subtitleTypo]}>Fecha inicio</Text>
          <Text style={[styles.subtitle, styles.subtitleTypo]}>{dateText}</Text>
          <Text style={[styles.subtitle2, styles.subtitleTypo]}>
            Fecha final
          </Text>
          <Text style={[styles.subtitle, styles.subtitleTypo]}>
            {eventDate}
          </Text>
        </View>
        <Image style={styles.itemChild} contentFit="cover" source={eventCode} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  listFlexBox: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "stretch",
  },
  subtitleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoRegular,
  },
  titleSpaceBlock: {
    marginLeft: 8,
    width: 101,
  },
  title: {
    color: Color.colorBlack,
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
    fontFamily: FontFamily.robotoRegular,
    width: 101,
  },
  subtitle: {
    fontSize: FontSize.size_xs,
    lineHeight: 16,
    color: Color.colorGray_500,
    textAlign: "left",
    fontFamily: FontFamily.robotoRegular,
    alignSelf: "stretch",
  },
  titleParent: {
    zIndex: 0,
    width: 101,
  },
  subtitle2: {
    width: 90,
    color: Color.colorBlack,
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
    fontFamily: FontFamily.robotoRegular,
  },
  titleGroup: {
    zIndex: 1,
  },
  titleContainer: {
    zIndex: 2,
  },
  itemChild: {
    position: "absolute",
    right: 0,
    bottom: -1,
    left: 0,
    maxWidth: "100%",
    overflow: "hidden",
    maxHeight: "100%",
    zIndex: 3,
  },
  item: {
    flexDirection: "row",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
  },
  list: {
    paddingHorizontal: Padding.p_xs,
    paddingVertical: 0,
  },
});

export default DateRangePickerContainer;
