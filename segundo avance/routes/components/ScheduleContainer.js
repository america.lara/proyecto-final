import * as React from "react";
import { Text, StyleSheet, View } from "react-native";
import { Image } from "expo-image";
import { FontFamily, FontSize, Color, Border, Padding } from "../GlobalStyles";

const ScheduleContainer = ({
  estimatedTime,
  estimatedTimeLabel,
  estimatedTimeDisplay,
  estimatedTimeFormatted,
  estimatedTimeText,
  estimatedTimeDescription,
  estimatedTimeLabelText,
}) => {
  return (
    <View style={[styles.list, styles.listFlexBox]}>
      <View style={[styles.item, styles.listFlexBox]}>
        <View style={styles.frame}>
          <Text style={[styles.icon, styles.iconPosition]} numberOfLines={1}>
            🚌
          </Text>
        </View>
        <View style={styles.titleParent}>
          <Text style={[styles.title, styles.titleFlexBox]}>
            Dax (Calle tercera)
          </Text>
          <Text style={[styles.subtitle, styles.titleFlexBox]}>
            {estimatedTime}
          </Text>
        </View>
        <Image
          style={[styles.itemChild, styles.iconPosition]}
          contentFit="cover"
          source={require("../assets/vector-2001.png")}
        />
      </View>
      <View style={[styles.item, styles.listFlexBox]}>
        <View style={styles.frame}>
          <Text style={[styles.icon, styles.iconPosition]} numberOfLines={1}>
            🚌
          </Text>
        </View>
        <View style={styles.titleParent}>
          <Text style={[styles.title, styles.titleFlexBox]}>
            Universidad IEE (Frente a Costco)
          </Text>
          <Text style={[styles.subtitle, styles.titleFlexBox]}>
            {estimatedTimeLabel}
          </Text>
        </View>
        <Image
          style={[styles.itemChild, styles.iconPosition]}
          contentFit="cover"
          source={require("../assets/vector-2001.png")}
        />
      </View>
      <View style={[styles.item, styles.listFlexBox]}>
        <View style={styles.frame}>
          <Text style={[styles.icon, styles.iconPosition]} numberOfLines={1}>
            🚌
          </Text>
        </View>
        <View style={styles.titleParent}>
          <Text style={[styles.title, styles.titleFlexBox]}>
            Sam's Club (Aviacion)
          </Text>
          <Text style={[styles.subtitle, styles.titleFlexBox]}>
            {estimatedTimeDisplay}
          </Text>
        </View>
        <Image
          style={[styles.itemChild, styles.iconPosition]}
          contentFit="cover"
          source={require("../assets/vector-2001.png")}
        />
      </View>
      <View style={[styles.item, styles.listFlexBox]}>
        <View style={styles.frame}>
          <Text style={[styles.icon, styles.iconPosition]} numberOfLines={1}>
            🚌
          </Text>
        </View>
        <View style={styles.titleParent}>
          <Text style={[styles.title, styles.titleFlexBox]}>
            Fenner Foods (Suc. 20 de Noviembre)
          </Text>
          <Text style={[styles.subtitle, styles.titleFlexBox]}>
            {estimatedTimeFormatted}
          </Text>
        </View>
        <Image
          style={[styles.itemChild, styles.iconPosition]}
          contentFit="cover"
          source={require("../assets/vector-2001.png")}
        />
      </View>
      <View style={[styles.item, styles.listFlexBox]}>
        <View style={styles.frame}>
          <Text style={[styles.icon, styles.iconPosition]} numberOfLines={1}>
            🚌
          </Text>
        </View>
        <View style={styles.titleParent}>
          <Text style={[styles.title, styles.titleFlexBox]}>
            Plaza Carrousel (Frente a la entrada)
          </Text>
          <Text style={[styles.subtitle, styles.titleFlexBox]}>
            {estimatedTimeText}
          </Text>
        </View>
        <Image
          style={[styles.itemChild, styles.iconPosition]}
          contentFit="cover"
          source={require("../assets/vector-2001.png")}
        />
      </View>
      <View style={[styles.item, styles.listFlexBox]}>
        <View style={styles.frame}>
          <Text style={[styles.icon, styles.iconPosition]} numberOfLines={1}>
            🚌
          </Text>
        </View>
        <View style={styles.titleParent}>
          <Text style={[styles.title, styles.titleFlexBox]}>
            Farmacia la mas barata (Lomas Verdes)
          </Text>
          <Text style={[styles.subtitle, styles.titleFlexBox]}>
            {estimatedTimeDescription}
          </Text>
        </View>
        <Image
          style={[styles.itemChild, styles.iconPosition]}
          contentFit="cover"
          source={require("../assets/vector-2001.png")}
        />
      </View>
      <View style={[styles.item, styles.listFlexBox]}>
        <View style={styles.frame}>
          <Text style={[styles.icon, styles.iconPosition]} numberOfLines={1}>
            🚌
          </Text>
        </View>
        <View style={styles.titleParent}>
          <Text style={[styles.title, styles.titleFlexBox]}>Haemonetics</Text>
          <Text style={[styles.subtitle, styles.titleFlexBox]}>
            {estimatedTimeLabelText}
          </Text>
        </View>
        <Image
          style={[styles.itemChild, styles.iconPosition]}
          contentFit="cover"
          source={require("../assets/vector-2001.png")}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  listFlexBox: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "stretch",
  },
  iconPosition: {
    overflow: "hidden",
    position: "absolute",
  },
  titleFlexBox: {
    textAlign: "left",
    fontFamily: FontFamily.robotoRegular,
    alignSelf: "stretch",
  },
  icon: {
    marginTop: -16,
    marginLeft: -16,
    top: "50%",
    left: "50%",
    fontSize: FontSize.size_xl,
    lineHeight: 32,
    textAlign: "center",
    display: "flex",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
    overflow: "hidden",
    position: "absolute",
    height: 32,
    width: 32,
    justifyContent: "center",
    alignItems: "center",
  },
  frame: {
    borderRadius: Border.br_base,
    backgroundColor: Color.colorGray_400,
    zIndex: 0,
    height: 32,
    width: 32,
  },
  title: {
    fontSize: FontSize.size_sm,
    lineHeight: 20,
    color: Color.colorBlack,
  },
  subtitle: {
    fontSize: FontSize.size_xs,
    lineHeight: 16,
    color: Color.colorGray_500,
  },
  titleParent: {
    flex: 1,
    zIndex: 1,
    marginLeft: 8,
  },
  itemChild: {
    right: 0,
    bottom: -1,
    left: 0,
    maxWidth: "100%",
    maxHeight: "100%",
    zIndex: 2,
    overflow: "hidden",
    position: "absolute",
  },
  item: {
    flexDirection: "row",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
  },
  list: {
    paddingHorizontal: Padding.p_xs,
    paddingVertical: 0,
    marginTop: 12,
  },
});

export default ScheduleContainer;
