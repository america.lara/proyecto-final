import * as React from "react";
import { Text, StyleSheet, View } from "react-native";
import { Image } from "expo-image";
import { FontFamily, FontSize, Color, Padding, Border } from "../GlobalStyles";

const ErrorReportingContainer = () => {
  return (
    <View style={[styles.list, styles.listFlexBox]}>
      <View style={[styles.item, styles.itemFlexBox]}>
        <Image
          style={styles.frameIcon}
          contentFit="cover"
          source={require("../assets/frame2.png")}
        />
        <View style={styles.titleParent}>
          <Text style={[styles.title1, styles.title1Typo]}>Reportar error</Text>
          <Text
            style={[styles.subtitle, styles.title1Typo]}
          >{`Envía un reporte para corregir un error en la aplicación `}</Text>
        </View>
        <Image
          style={styles.itemChild}
          contentFit="cover"
          source={require("../assets/vector-2001.png")}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  listFlexBox: {
    justifyContent: "center",
    alignSelf: "stretch",
  },
  itemFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  title1Typo: {
    fontFamily: FontFamily.robotoRegular,
    textAlign: "left",
    alignSelf: "stretch",
  },
  title: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    textAlign: "left",
    color: Color.colorBlack,
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    width: 359,
    paddingTop: Padding.p_base,
    paddingHorizontal: Padding.p_xs,
  },
  frameIcon: {
    borderRadius: Border.br_base,
    width: 32,
    height: 32,
    zIndex: 0,
  },
  title1: {
    fontSize: FontSize.size_sm,
    lineHeight: 20,
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
  },
  subtitle: {
    fontSize: FontSize.size_xs,
    lineHeight: 16,
    color: Color.colorGray_500,
  },
  titleParent: {
    zIndex: 1,
    marginLeft: 8,
    flex: 1,
  },
  itemChild: {
    position: "absolute",
    right: 0,
    bottom: -1,
    left: 0,
    maxWidth: "100%",
    overflow: "hidden",
    maxHeight: "100%",
    zIndex: 2,
  },
  item: {
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
    justifyContent: "center",
    alignSelf: "stretch",
    flexDirection: "row",
  },
  list: {
    paddingVertical: 0,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "stretch",
  },
});

export default ErrorReportingContainer;
