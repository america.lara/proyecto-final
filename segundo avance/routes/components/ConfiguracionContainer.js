import React, { useMemo } from "react";
import { Text, StyleSheet, View } from "react-native";
import { Image } from "expo-image";
import { Color, FontFamily, FontSize, Border, Padding } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const ConfiguracionContainer = ({
  iconEmoji,
  menuOptionText,
  propAlignSelf,
  propWidth,
  propColor,
}) => {
  const itemStyle = useMemo(() => {
    return {
      ...getStyleValue("alignSelf", propAlignSelf),
      ...getStyleValue("width", propWidth),
    };
  }, [propAlignSelf, propWidth]);

  const title2Style = useMemo(() => {
    return {
      ...getStyleValue("color", propColor),
    };
  }, [propColor]);

  return (
    <View style={[styles.item, styles.itemFlexBox, itemStyle]}>
      <View style={[styles.frame, styles.iconLayout]}>
        <Text style={[styles.icon, styles.iconPosition]} numberOfLines={1}>
          {iconEmoji}
        </Text>
      </View>
      <View style={styles.titleWrapper}>
        <Text style={[styles.title, styles.iconClr, title2Style]}>
          {menuOptionText}
        </Text>
      </View>
      <Image
        style={[styles.itemChild, styles.iconPosition]}
        contentFit="cover"
        source={require("../assets/vector-2001.png")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  itemFlexBox: {
    justifyContent: "center",
    alignItems: "center",
  },
  iconLayout: {
    height: 32,
    width: 32,
  },
  iconPosition: {
    overflow: "hidden",
    position: "absolute",
  },
  iconClr: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
  },
  icon: {
    marginTop: -16,
    marginLeft: -16,
    top: "50%",
    left: "50%",
    fontSize: FontSize.size_xl,
    lineHeight: 32,
    textAlign: "center",
    display: "flex",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
    height: 32,
    width: 32,
    justifyContent: "center",
    alignItems: "center",
  },
  frame: {
    borderRadius: Border.br_base,
    backgroundColor: Color.colorGray_400,
    zIndex: 0,
  },
  title: {
    fontSize: FontSize.size_sm,
    lineHeight: 20,
    textAlign: "left",
    alignSelf: "stretch",
  },
  titleWrapper: {
    flex: 1,
    zIndex: 1,
    marginLeft: 8,
  },
  itemChild: {
    right: 0,
    bottom: -1,
    left: 0,
    maxWidth: "100%",
    maxHeight: "100%",
    zIndex: 2,
  },
  item: {
    flexDirection: "row",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
  },
});

export default ConfiguracionContainer;
