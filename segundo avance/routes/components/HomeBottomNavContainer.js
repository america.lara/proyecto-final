import React, { useMemo } from "react";
import { Image } from "expo-image";
import { useNavigation } from '@react-navigation/native'; 
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Padding, Color, FontFamily, FontSize } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const HomeBottomNavContainer = ({
  dimensionsCode,
  propOverflow,
  propMarginTop,
  propHeight,
  propMarginLeft,
}) => {
  const navigation = useNavigation();

  const irAMenu = () => {
    navigation.navigate('Menu'); // Navegar a la pantalla de Menú
  };

  const irAInicio = () => {
    navigation.navigate('Principal'); // Navegar a la pantalla de Inicio
  };

  const bottomNavStyle = useMemo(() => {
    return {
      ...getStyleValue("overflow", propOverflow),
      ...getStyleValue("marginTop", propMarginTop),
    };
  }, [propOverflow, propMarginTop]);

  const tabStyle = useMemo(() => {
    return {
      ...getStyleValue("height", propHeight),
      ...getStyleValue("marginLeft", propMarginLeft),
    };
  }, [propHeight, propMarginLeft]);

  return (
    <View style={[styles.bottomNav, bottomNavStyle]}>
      {/* Botón Menú */}
      <TouchableOpacity onPress={irAMenu} style={styles.tab}>
      <View style={[styles.tab, styles.tabFlexBox]}>
            <Image
              style={styles.image6Icon}
              contentFit="cover"
              source={require('../assets/image-6.png')}
            />
          <Text style={[styles.title, styles.iconFlexBox]} numberOfLines={1}>
            Menú
          </Text>
        </View>
      </TouchableOpacity>
      {/* Botón Inicio */}
      <View onPress={irAInicio} style={[styles.bottomNavButton]}>
      <View style={styles.bottomNav}>
            <View style={styles.tab}>            
            <Image
              style={styles.image6Icon}
              contentFit="cover"
              source={require('../assets/casa.png')}
            />
          <Text style={[styles.title, styles.iconFlexBox]} numberOfLines={1}>
            Inicio
          </Text>
        </View>
        </View>
        </View>
    </View>
  );
};


const styles = StyleSheet.create({
  tabFlexBox: {
    padding: Padding.p_9xs,
    alignItems: "center",
    flex: 1,
  },
  bottomNavButton: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
  },
  iconFlexBox: {
    display: "flex",
    textAlign: "center",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
  },
  image6Icon: {
    width: 21,
    height: 21,
  },
  title: {
    alignSelf: "stretch",
    fontSize: FontSize.size_3xs,
    lineHeight: 14,
    height: 14,
  },
  tab: {
    paddingVertical: Padding.p_9xs,
    height: 53,
    flex: 1,
    alignItems: 'center',
  },
  icon: {
    fontSize: FontSize.size_xl,
    lineHeight: 28,
    width: 21,
    height: 21,
  },
  tab1: {
    padding: Padding.p_9xs,
    alignItems: "center",
    flex: 1,
  },
  bottomNav: {
    paddingHorizontal: 123,
    paddingVertical: Padding.p_base,
    overflow: "hidden",
    shadowOpacity: 1,
    elevation: 6,
    shadowRadius: 6,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowColor: "rgba(0, 0, 0, 0.12)",
    backgroundColor: Color.colorWhite,
  },
});

export default HomeBottomNavContainer;
