import * as React from "react";
import { Text, StyleSheet, View } from "react-native";
import { Padding, Border, Color, FontFamily, FontSize } from "../GlobalStyles";

const ConfirmacionCierreDeSesion = () => {
  return (
    <View style={[styles.confirmacionCierreDeSesion, styles.cardFlexBox]}>
      <View style={[styles.card, styles.cardBorder]}>
        <View style={styles.image}>
          <View style={[styles.button, styles.buttonSpaceBlock]}>
            <View style={[styles.seconday, styles.secondayFlexBox]}>
              <Text style={[styles.title, styles.titleTypo]}>Salir</Text>
            </View>
          </View>
          <View style={[styles.button1, styles.buttonSpaceBlock]}>
            <View style={styles.secondayFlexBox}>
              <Text style={[styles.title1, styles.titleTypo]}>Cancelar</Text>
            </View>
          </View>
          <Text style={styles.subtitle}>
            ¿Está seguro que desea cerrar su sesión?
          </Text>
        </View>
        <View style={styles.imageContainer} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardFlexBox: {
    alignItems: "center",
    height: 210,
  },
  cardBorder: {
    borderWidth: 1,
    borderStyle: "solid",
  },
  buttonSpaceBlock: {
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
    left: 0,
    position: "absolute",
    width: 360,
  },
  secondayFlexBox: {
    paddingVertical: Padding.p_3xs,
    justifyContent: "center",
    borderRadius: Border.br_5xs,
    flex: 1,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    backgroundColor: Color.colorWhite,
  },
  titleTypo: {
    textAlign: "left",
    lineHeight: 22,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    fontSize: FontSize.size_base,
  },
  title: {
    color: Color.colorRed,
  },
  seconday: {
    borderColor: Color.colorBlack,
    borderWidth: 1,
    borderStyle: "solid",
  },
  button: {
    top: 70,
    overflow: "hidden",
  },
  title1: {
    color: Color.colorWhite,
  },
  button1: {
    top: 129,
  },
  subtitle: {
    marginLeft: -171,
    bottom: 157,
    left: "50%",
    lineHeight: 24,
    color: Color.colorBlack,
    textAlign: "center",
    width: 342,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    fontSize: FontSize.size_base,
    position: "absolute",
  },
  image: {
    backgroundColor: Color.colorGray_400,
    width: 357,
    height: 210,
  },
  imageContainer: {
    alignSelf: "stretch",
    height: 150,
    overflow: "hidden",
  },
  card: {
    borderRadius: Border.br_7xs,
    borderColor: Color.colorGray_200,
    width: 358,
    overflow: "hidden",
    alignItems: "center",
    height: 210,
  },
  confirmacionCierreDeSesion: {
    paddingBottom: Padding.p_xs,
    width: 360,
    backgroundColor: Color.colorWhite,
    alignItems: "center",
  },
});

export default ConfirmacionCierreDeSesion;
