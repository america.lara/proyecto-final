import * as React from "react";
import { Image, TouchableOpacity, View, SafeAreaView } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { StyleSheet, Text } from "react-native";
import UserAvatarContainer from "../components/UserAvatarContainer";
import DateRangePickerContainer from "../components/DateRangePickerContainer";
import HomeBottomNavContainer from "../components/HomeBottomNavContainer";
import { FontFamily, Color, FontSize, Padding } from "../GlobalStyles";

const Perfil = () => {
  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.perfil}>
        <View style={styles.topBar}>
          <View style={[styles.content, styles.buttonFlexBox]}>
            <TouchableOpacity onPress={irAtras}>
              <Image
                style={styles.icLeftIcon}
                contentFit="cover"
                source={require("../assets/icleft.png")}
              />
            </TouchableOpacity>
            <Text style={[styles.title, styles.titleTypo]}>Perfil</Text>
          </View>
        </View>
        <View style={styles.contentT}>
          <UserAvatarContainer
            itemCode={require("../assets/avatar1.png")}
            propMarginTop="unset"
          />
        </View>
        <View style={[styles.sectionTitle1, styles.buttonFlexBox]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>
              Información del empleado
            </Text>
          </View>
        </View>
        <View style={[styles.list, styles.listFlexBox]}>
          <View style={[styles.item, styles.listFlexBox]}>
            <View style={styles.subtitleWrapper}>
              <Text style={[styles.subtitle, styles.title3Typo]}>
                Numero de empleado
              </Text>
            </View>
            <Image
              style={[styles.itemChild, styles.topIconLayout]}
              contentFit="cover"
              source={require("../assets/vector-2002.png")}
            />
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.buttonFlexBox]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>
              Configuración de contraseña
            </Text>
          </View>
        </View>
        <View style={[styles.button, styles.buttonFlexBox]}>
          <Text style={[styles.title3, styles.title3Typo]}>
            Cambiar contraseña
          </Text>
          <Image
            style={styles.icon}
            contentFit="cover"
            source={require("../assets/icon.png")}
          />
        </View>
        <View style={[styles.sectionTitle, styles.buttonFlexBox]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>Historial</Text>
          </View>
        </View>
        <View style={styles.borde}>
          <DateRangePickerContainer
            dateText="01/02/2024"
            eventDate="10/02/2024"
            eventCode={require("../assets/vector-2003.png")}
          />
          <DateRangePickerContainer
            dateText="21/01/2024"
            eventDate="30/01/2024"
            eventCode={require("../assets/vector-2004.png")}
          />
          <DateRangePickerContainer
            dateText="11/01/2024"
            eventDate="20/01/2024"
            eventCode={require("../assets/vector-2005.png")}
          />
          <DateRangePickerContainer
            dateText="01/01/2024"
            eventDate="10/01/2024"
            eventCode={require("../assets/vector-2006.png")}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  topIconLayout: {
    overflow: "hidden",
    maxWidth: "100%",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  buttonFlexBox: {
    alignItems: "center",
    flexDirection: "row",
  },
  borde: {
    borderWidth: 1,
    borderColor: "#000",
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    color: Color.colorBlack,
  },
  listFlexBox: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "stretch",
  },
  title3Typo: {
    fontFamily: FontFamily.robotoRegular,
    lineHeight: 16,
    fontSize: FontSize.size_xs,
    textAlign: "left",
  },
  topIcon: {
    height: 24,
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    flex: 1,
  },
  content: {
    paddingLeft: Padding.p_base,
    paddingTop: Padding.p_xs,
    paddingRight: Padding.p_5xs,
    alignSelf: "stretch",
    paddingBottom: Padding.p_xs,
  },
  contentT: {
    paddingTop: 8,
    paddingRight: Padding.p_5xs,
    paddingBottom: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingBottom: 8,
    paddingTop: Padding.p_base,
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  sectionTitle1: {
    paddingTop: 8,
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  subtitle: {
    color: Color.colorGray_500,
    alignSelf: "stretch",
  },
  subtitleWrapper: {
    zIndex: 0,
    flex: 1,
  },
  itemChild: {
    position: "absolute",
    right: 0,
    bottom: -1,
    left: 0,
    maxHeight: "100%",
    zIndex: 1,
  },
  item: {
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
    flexDirection: "row",
    justifyContent: "center",
  },
  list: {
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
  },
  title3: {
    color: Color.colorBlack,
    lineHeight: 16,
    fontSize: FontSize.size_xs,
  },
  icon: {
    width: 12,
    height: 12,
    marginLeft: 2,
  },
  button: {
    borderRadius: 4,
    borderStyle: "solid",
    borderColor: Color.colorBlack,
    borderWidth: 1,
    paddingLeft: Padding.p_5xs,
    paddingTop: Padding.p_10xs,
    paddingRight: Padding.p_9xs,
    paddingBottom: Padding.p_10xs,
  },
  perfil: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
  },
});

export default Perfil;
