import * as React from "react";
import { Image } from "expo-image";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import PasswordFormButtonContainer from "../components/PasswordFormButtonContainer";
import { Padding, FontSize, Color, FontFamily, Border } from "../GlobalStyles";

const RegistroDeEmpleado = () => {
  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.registroDeEmpleado}>
        <View style={[styles.content]}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Registro de empleado</Text>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Nombre de pila</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su nombre
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Primer Apellido</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su primer apellido
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Segundo Apellido</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su segundo apellido (opcional)
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Calle</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su calle
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Número exterior</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su número exterior
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Colonia</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su colonia
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>
            Número de teléfono
          </Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su número de teléfono
            </Text>
          </View>
        </View>
        <PasswordFormButtonContainer
          resetPasswordBtnText="Cancelar"
          actionButtonText="Registrar"
          propHeight="unset"
          propHeight1="unset"
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  contentFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  inputSpaceBlock: {
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  textTypo: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    lineHeight: 24,
    marginLeft: 8,
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    alignSelf: "stretch",
  },
  text: {
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorGray_500,
    height: 20,
    overflow: "hidden",
    fontSize: FontSize.size_sm,
    flex: 1,
  },
  textfield: {
    borderRadius: Border.br_7xs,
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    paddingVertical: Padding.p_5xs,
    marginTop: 4,
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    justifyContent: "center",
    paddingVertical: 0,
    marginTop: 12,
    overflow: "hidden",
  },
  registroDeEmpleado: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default RegistroDeEmpleado;
