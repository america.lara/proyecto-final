import * as React from "react";
import { Image } from "expo-image";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import ImageUploadContainer from "../components/ImageUploadContainer";
import PasswordFormButtonContainer from "../components/PasswordFormButtonContainer";
import HomeBottomNavContainer from "../components/HomeBottomNavContainer";
import { Color, FontFamily, FontSize, Padding, Border } from "../GlobalStyles";

const ReporteDeErrores = () => {
  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };
  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.reporteDeErrores}>
        <View style={[styles.content, styles.contentFlexBox]}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={[styles.title, styles.titleTypo]}>
            Reporte de errores
          </Text>
        </View>
        <View style={[styles.sectionTitle, styles.inputSpaceBlock]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>
              Reportar error
            </Text>
            <Text style={[styles.subtitle, styles.infoClr]}>
              Si necesita ayuda, facilítenos los siguientes datos
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title2, styles.text1Typo]}>
            Descripción de error
          </Text>
          <View style={[styles.textfield, styles.infoSpaceBlock]}>
            <Text style={[styles.text1, styles.text1Typo]} numberOfLines={1}>
              Introduzca la descripción del error
            </Text>
          </View>
          <Text style={[styles.info, styles.infoSpaceBlock]}>
            Por favor, sea lo más específico posible
          </Text>
        </View>
        <View style={[styles.sectionTitle, styles.inputSpaceBlock]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>
              Captura de pantalla del error o fallo
            </Text>
          </View>
        </View>
        <ImageUploadContainer />
        <PasswordFormButtonContainer
          resetPasswordBtnText="Cancelar"
          actionButtonText="Enviar"
          propHeight="unset"
          propHeight1="unset"
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  contentFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  titleTypo: {
    textAlign: "left",
    lineHeight: 24,
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
  },
  inputSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  infoClr: {
    color: Color.colorGray_500,
    fontFamily: FontFamily.robotoRegular,
  },
  text1Typo: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
  },
  infoSpaceBlock: {
    marginTop: 4,
    alignSelf: "stretch",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  subtitle: {
    lineHeight: 16,
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.robotoRegular,
    textAlign: "left",
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
    alignItems: "center",
  },
  title2: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    alignSelf: "stretch",
  },
  text1: {
    height: 20,
    color: Color.colorGray_500,
    fontFamily: FontFamily.robotoRegular,
    overflow: "hidden",
    flex: 1,
  },
  textfield: {
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    paddingVertical: Padding.p_5xs,
    borderRadius: Border.br_7xs,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
    alignItems: "center",
  },
  info: {
    color: Color.colorGray_500,
    fontFamily: FontFamily.robotoRegular,
    lineHeight: 16,
    fontSize: FontSize.size_xs,
    textAlign: "left",
  },
  input: {
    justifyContent: "center",
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
    overflow: "hidden",
  },
  image: {
    backgroundColor: Color.colorGray_300,
    height: 11,
    borderRadius: Border.br_7xs,
  },
  reporteDeErrores: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default ReporteDeErrores;
