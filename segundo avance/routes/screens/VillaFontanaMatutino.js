import * as React from "react";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import BusScheduleContainer from "../components/BusScheduleContainer";
import HomeBottomNavContainer from "../components/HomeBottomNavContainer";
import { Color, FontFamily, FontSize, Padding, Border } from "../GlobalStyles";

const VillaFontanaMatutino = () => {
  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.villaFontanaMatutino}>
        <View style={[styles.content, styles.contentFlexBox]}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={[styles.title, styles.titleTypo]}>
            Horarios de parada
          </Text>
        </View>
        <View style={[styles.sectionTitle, styles.imageSpaceBlock]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>
              Villa Fontana (Matutino)
            </Text>
          </View>
        </View>
        <BusScheduleContainer
          parkLocationName="Parque Villa Fontana (Entrada)"
          estimatedTime="Hora estimada: 5:30 AM"
          locationDescription="Plaza 2000 (Frente a Chevron)"
          estimatedTimeDescription="Hora estimada: 5:45 AM"
          storeName="Farmacia Roma (Terrazas del Valle)"
          estimatedTimeLabel="Hora estimada: 6:00 AM"
          locationName="Farmacia YZA (Refugio)"
          estimatedTimeLabelText="Hora estimada: 6:20 AM"
          parkName="Puente De Las Beta"
          estimatedTimeDescriptionT="Hora estimada: 6:35 AM"
          estimatedTimeDisplay="Hora estimada: 6:50 AM"
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  contentFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  titleTypo: {
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
  },
  imageSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  topIcon: {
    maxWidth: "100%",
    overflow: "hidden",
    height: 24,
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingHorizontal: Padding.p_xs,
    paddingTop: Padding.p_base,
    flexDirection: "row",
    alignItems: "center",
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    height: 225,
  },
  villaFontanaMatutino: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default VillaFontanaMatutino;
