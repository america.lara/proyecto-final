import React from "react";
import { useNavigation } from "@react-navigation/native";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from "react-native";
import { FontSize, FontFamily, Color, Padding, Border } from "../GlobalStyles";

const CerrarSesion = () => {
  const navigation = useNavigation();

  const irAtras = () => {
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.prox}>
        <View style={styles.topBar}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Proximamente</Text>
        </View>
        <View style={styles.container}>
          <View style={[styles.confirmacionCierreDeSesion, styles.cardFlexBox]}>
            <View style={[styles.card, styles.cardBorder]}>
              <Text style={styles.subtitle}>PROXIMAMENTE...</Text>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  topBar: {
    flexDirection: "row",
    alignItems: "center",
    padding: Padding.p_5xs,
    backgroundColor: Color.colorWhite,
    elevation: 6,
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 6,
    shadowOpacity: 1,
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  prox: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  confirmacionCierreDeSesion: {
    width: "80%",
    maxWidth: 300,
  },
  card: {
    padding: 20,
    borderRadius: 10,
    backgroundColor: "#fff",
  },
  cardBorder: {
    borderWidth: 1,
    borderColor: "#ccc",
  },
  subtitle: {
    marginBottom: 10,
    fontSize: 18,
    textAlign: "center",
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  button: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    alignItems: "center",
  },
  secondary: {
    backgroundColor: "#000",
  },
  title: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
    alignSelf: "stretch",
  },
  titleTypo: {
    fontFamily: "Roboto",
    fontWeight: "bold",
  },
});

export default CerrarSesion;
