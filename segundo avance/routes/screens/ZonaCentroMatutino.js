import * as React from "react";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import ScheduleContainer from "../components/ScheduleContainer";
import HomeBottomNavContainer from "../components/HomeBottomNavContainer";
import { Color, FontFamily, FontSize, Padding, Border } from "../GlobalStyles";

const ZonaCentroMatutino = () => {
  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.zonaCentroMatutino}>
        <View style={styles.topBar}>
          <View style={[styles.content, styles.contentFlexBox]}>
            <TouchableOpacity onPress={irAtras}>
              <Image
                style={styles.icLeftIcon}
                contentFit="cover"
                source={require("../assets/icleft.png")}
              />
            </TouchableOpacity>
            <Text style={[styles.title, styles.titleTypo]}>
              Horarios de parada
            </Text>
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.imageSpaceBlock]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>
              Zona Centro (Matutino)
            </Text>
          </View>
        </View>
        <ScheduleContainer
          estimatedTime="Hora estimada: 5:30 AM"
          estimatedTimeLabel="Hora estimada: 5:40 AM"
          estimatedTimeDisplay="Hora estimada: 5:55 AM"
          estimatedTimeFormatted="Hora estimada: 6:05 AM"
          estimatedTimeText="Hora estimada: 6:20 AM"
          estimatedTimeDescription="Hora estimada: 6:35 AM"
          estimatedTimeLabelText="Hora estimada: 6:50 AM"
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  contentFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  titleTypo: {
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
  },
  imageSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  topIcon: {
    maxWidth: "100%",
    overflow: "hidden",
    height: 24,
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingHorizontal: Padding.p_xs,
    paddingTop: Padding.p_base,
    flexDirection: "row",
    alignItems: "center",
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    height: 164,
  },
  zonaCentroMatutino: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default ZonaCentroMatutino;
