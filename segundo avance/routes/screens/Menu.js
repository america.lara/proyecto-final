import * as React from "react";
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from "react-native";
import ConfiguracionContainer from "../components/ConfiguracionContainer";
import { FontSize, FontFamily, Color, Padding, Border } from "../GlobalStyles";
import { useNavigation } from "@react-navigation/native";

const Menu = () => {
  const navigation = useNavigation();

  const irAInicio = () => {
    navigation.navigate("Principal");
  };
  const irAAjustes = () => {
    navigation.navigate("Ajustes");
  };
  const irAPerfil = () => {
    navigation.navigate("Perfil");
  };
  const irARutas = () => {
    navigation.navigate("Rutas");
  };
  const irACerrarSesion = () => {
    navigation.navigate("CerrarSesion");
  };
  const irAtras = () => {
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.menu}>
        <View style={styles.topBar}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Menu</Text>
        </View>
        <View style={styles.listSpaceBlock}>
          <TouchableOpacity onPress={irAAjustes}>
            <ConfiguracionContainer
              iconEmoji="⚙️"
              menuOptionText="Configuración"
              propAlignSelf="stretch"
              propWidth="unset"
              propColor="#000"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irARutas}>
            <ConfiguracionContainer
              iconEmoji="🔍"
              menuOptionText="Ver rutas"
              propAlignSelf="stretch"
              propWidth="unset"
              propColor="#000"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irAPerfil}>
            <ConfiguracionContainer
              iconEmoji="👤"
              menuOptionText="Perfil"
              propAlignSelf="stretch"
              propWidth="unset"
              propColor="#000"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irACerrarSesion}>
            <ConfiguracionContainer
              iconEmoji="🔒"
              menuOptionText="Cerrar sesión "
              propAlignSelf="unset"
              propWidth={336}
              propColor="#000"
            />
          </TouchableOpacity>
        </View>
        <View style={[styles.image, styles.listSpaceBlock]} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  buttonFlexBox: {
    alignItems: "center",
    flexDirection: "row",
  },
  content: {
    paddingLeft: Padding.p_base,
    paddingTop: Padding.p_xs,
    paddingRight: Padding.p_5xs,
    alignSelf: "stretch",
    paddingBottom: Padding.p_xs,
  },
  topBar: {
    flexDirection: "row",
    alignItems: "center",
    padding: Padding.p_5xs,
    backgroundColor: Color.colorWhite,
    elevation: 6,
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 6,
    shadowOpacity: 1,
  },
  menu: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
  listSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  sectionTitle: {
    flexDirection: "row",
    paddingHorizontal: Padding.p_xs,
    marginTop: 12,
    alignItems: "center",
  },
  title: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  image6Icon: {
    width: 21,
    height: 21,
  },
  list: {
    justifyContent: "center",
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
    marginTop: 12,
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    flex: 1,
  },
  bottomNavButton: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: "center",
  },
  bottomNav: {
    paddingHorizontal: 123,
    paddingVertical: Padding.p_base,
    overflow: "hidden",
    shadowOpacity: 1,
    elevation: 6,
    shadowRadius: 6,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowColor: "rgba(0, 0, 0, 0.12)",
    backgroundColor: Color.colorWhite,
  },
  tab: {
    paddingVertical: Padding.p_9xs,
    height: 53,
    flex: 1,
    alignItems: "center",
  },
  title2: {
    fontSize: FontSize.size_3xs,
    lineHeight: 14,
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
    textAlign: "center",
    justifyContent: "center",
    overflow: "hidden",
    alignSelf: "stretch",
  },
});

export default Menu;
