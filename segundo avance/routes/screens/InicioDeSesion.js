import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  TextInput,
} from "react-native";
import { FontSize, FontFamily, Color, Padding, Border } from "../GlobalStyles";

import { useNavigation } from "@react-navigation/native";

const InicioDeSesion = () => {
  const [empleado, setEmpleado] = useState("");
  const [contra, setContra] = useState("");

  const navigation = useNavigation();

  const irAPrincipal = () => {
    navigation.navigate("Principal");
  };

  const irAMenuAdmin = () => {
    navigation.navigate("MenuAdministradores");
  };

  const irASoporte = () => {
    navigation.navigate("SoporteTecnico");
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.sesion}>
        <View style={styles.container}>
          <Text style={styles.title}>Inicio de sesión</Text>

          <View style={[styles.input, styles.inputSpaceBlock]}>
            <Text style={[styles.title2, styles.text1Typo]}>
              Numero de empleado
            </Text>
            <View style={[styles.textfield, styles.textfieldBorder]}>
              <TextInput
                style={[styles.text1, styles.text1Typo]}
                value={empleado}
                onChangeText={setEmpleado}
                placeholder="XXXXXXXXXX"
                keyboardType="numeric"
              />
            </View>
          </View>
          <View style={[styles.input, styles.inputSpaceBlock]}>
            <Text style={[styles.title2, styles.text1Typo]}>
              Ingrese su contraseña
            </Text>
            <View style={[styles.textfield, styles.textfieldBorder]}>
              <TextInput
                style={[styles.text1, styles.text1Typo]}
                value={contra}
                onChangeText={setContra}
                placeholder="Contraseña"
                keyboardType="default"
              />
            </View>
          </View>

          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={irAPrincipal} style={styles.button1}>
              <Text style={styles.buttonText1}>Iniciar Sesion</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={irAMenuAdmin} style={styles.button2}>
              <Text style={styles.buttonText2}>Ir a Menu</Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity onPress={irASoporte} style={styles.supportButton}>
            <View style={[styles.bottomNav]}>
              <View style={styles.tab}>
                <Image
                  style={styles.image6Icon}
                  contentFit="cover"
                  source={require("../assets/image-61.png")}
                />
                <Text style={[styles.title3]} numberOfLines={1}>
                  Soporte Técnico
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  text1: {
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorGray_500,
    height: 20,
    overflow: "hidden",
    flex: 1,
  },
  textfieldBorder: {
    marginTop: 4,
    borderWidth: 1,
    borderColor: Color.colorGray_200,
    borderStyle: "solid",
    borderRadius: Border.br_7xs,
    alignSelf: "stretch",
    alignItems: "center",
  },
  textfield: {
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
  },
  text1Typo: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
  },
  inputSpaceBlock: {
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
    overflow: "hidden",
    alignSelf: "stretch",
  },
  sesion: {
    flex: 1,
    paddingBottom: Padding.p_xs,
  },
  title2: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    alignSelf: "stretch",
  },
  safeArea: {
    flex: 1,
    backgroundColor: "#fff",
  },
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: Padding.p_xs,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  input: {
    marginBottom: 20,
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
    padding: 10,
  },
  placeholder: {
    color: "#999",
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20,
  },
  button1: {
    backgroundColor: "#000",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#000",
  },
  button2: {
    backgroundColor: "#fff",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#000",
  },
  buttonText1: {
    color: "#fff",
    fontSize: 16,
    textAlign: "center",
  },
  buttonText2: {
    color: "#000",
    fontSize: 16,
    textAlign: "center",
  },
  supportButton: {
    width: "auto",
    height: "auto",
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: "center",
  },
  supportButtonText: {
    color: "#fff",
    fontSize: 16,
    textAlign: "center",
  },
  bottomNav: {
    width: "auto",
    height: "auto",
    marginTop: 12,
    overflow: "hidden",
  },
  tab: {
    padding: Padding.p_9xs,
    height: 53,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  image6Icon: {
    width: 29,
    height: 29,
  },
  title3: {
    fontSize: FontSize.size_3xs,
    lineHeight: 14,
    textAlign: "center",
    display: "flex",
    height: 14,
    justifyContent: "center",
    color: Color.colorBlack,
    alignSelf: "stretch",
    alignItems: "center",
  },
});

export default InicioDeSesion;
