import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { useNavigation } from "@react-navigation/native";

const CerrarSesion = () => {
  const navigation = useNavigation();

  const irAMenuAdmin = () => {
    navigation.navigate("MenuAdministradores");
  };

  const irAInicioSesion = () => {
    navigation.navigate("InicioDeSesion");
  };

  return (
    <View style={styles.container}>
      <View style={[styles.confirmacionCierreDeSesion, styles.cardFlexBox]}>
        <View style={[styles.card, styles.cardBorder]}>
          <Text style={styles.subtitle}>
            ¿Está seguro que desea cerrar su sesión?
          </Text>
          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={irAMenuAdmin}>
              <View style={[styles.button, styles.secondary]}>
                <Text style={[styles.title, styles.titleTypo]}>Cancelar</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.buttonContainer1}>
            <TouchableOpacity onPress={irAInicioSesion}>
              <View style={[styles.button, styles.secondary1]}>
                <Text style={[styles.title1, styles.titleTypo]}>
                  Cerrar sesion
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  confirmacionCierreDeSesion: {
    width: "80%",
    maxWidth: 300,
  },
  card: {
    padding: 20,
    borderRadius: 10,
    backgroundColor: "#fff",
  },
  cardBorder: {
    borderWidth: 1,
    borderColor: "#ccc",
  },
  subtitle: {
    marginBottom: 10,
    fontSize: 18,
    textAlign: "center",
  },
  buttonContainer: {
    paddingBottom: 14,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  buttonContainer1: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  button: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#000",
  },
  secondary: {
    backgroundColor: "#000",
  },
  secondary1: {
    backgroundColor: "#fff",
  },
  title: {
    color: "#fff",
    fontSize: 16,
  },
  title1: {
    color: "#000",
    fontSize: 16,
  },
  titleTypo: {
    fontFamily: "Roboto",
    fontWeight: "bold",
  },
});

export default CerrarSesion;
